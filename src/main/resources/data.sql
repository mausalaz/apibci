DROP TABLE IF EXISTS PHONE;
DROP TABLE IF EXISTS USER;
 
CREATE TABLE USER (
  id IDENTITY NOT NULL PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  isactive INT DEFAULT NULL,
  created date,
  modified date,
  last_login timestamp,
  token VARCHAR(400)
);

CREATE TABLE PHONE (
  id_phone IDENTITY NOT NULL PRIMARY KEY,
  number VARCHAR(20) NOT NULL,
  citycode VARCHAR(20) NOT NULL,
  contrycode VARCHAR(20) DEFAULT NULL,
  user_id int
);

INSERT INTO USER (name, email,password, isactive,created,modified, last_login, token) VALUES
 ('Mauricio Salazar', 'mg.salazar.perez@gmail.com','$2a$12$XXvqNoWxnmut9NF50u5EOO7.RwrK0wx7YG0hLDM5k1BH2RoPZUn3i', 1, SYSDATE, null, SYSDATE, null);
 --('Cristian Badilla', 'dbadilla@gmail.com', 0, SYSDATE, SYSDATE, SYSDATE);
 
 --insert into PHONE (number, citycode, contrycode, user_id) values
  --('432432', '15', '43', 1)