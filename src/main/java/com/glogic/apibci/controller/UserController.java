package com.glogic.apibci.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glogic.apibci.dto.UserDTO;
import com.glogic.apibci.entity.User;
import com.glogic.apibci.exception.ErrorFormatMailException;
import com.glogic.apibci.exception.FormatPasswordNotValid;
import com.glogic.apibci.exception.RegisteredEmailException;
import com.glogic.apibci.exception.SaveTokenException;
import com.glogic.apibci.exception.UserNotFound;
import com.glogic.apibci.service.UserService;


@RestController
public class UserController{

	protected ResponseEntity<User> responseEntity = null;
    
    private final UserService userService;
//    private final PhoneService phoneService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService
//    					 ,PhoneService phoneService
    		) {
        this.userService = userService;
//        this.phoneService = phoneService;
    }
    
	@PostMapping(value = "/login", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> login(@RequestParam("email") String email, @RequestParam("password") String password) throws UserNotFound, SaveTokenException {
		return userService.findByMailAndPassword(email, password);
		
	}

    
    @PostMapping(value = "/registro", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<UserDTO> addUser( @Validated @RequestBody User userBody) throws ErrorFormatMailException, FormatPasswordNotValid, RegisteredEmailException {
    	UserDTO userResponse = userService.save(userBody);
    	LOGGER.info("userResponse", userResponse);
//    	userService.findAll();
//        phoneService.findAll();
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }
    
    @GetMapping("/allUsers")
    public ResponseEntity<List<User>> getAllUsers() {
    	return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }
    
//    @GetMapping(value = "/edit/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
//    public ResponseEntity<User> showUpdateForm(@PathVariable("id") long id) {
//        User user = userService.findById(id);
//        return new ResponseEntity<>(user, HttpStatus.OK);
//    }
    
//    @PostMapping("/update/{id}")
//    public ResponseEntity<User> updateUser(@RequestBody User userBody) {
//    	 User user = userService.update(userBody);
//         return new ResponseEntity<>(user, HttpStatus.OK);
//    }
    
//    @GetMapping("/delete/{id}")
//    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
//    	userService.delete(id);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
    
}
