package com.glogic.apibci.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.glogic.apibci.exception.ErrorFormatMailException;
import com.glogic.apibci.exception.FormatPasswordNotValid;
import com.glogic.apibci.exception.RegisteredEmailException;
import com.glogic.apibci.exception.SaveTokenException;
import com.glogic.apibci.exception.UserNotFound;
import com.glogic.apibci.util.UtilsBean;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	
    /**
     * this method catches every ErrorFormatMailException to send a custom error json response
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exceptionGeneral(Exception e, HttpServletRequest http) {
    	LOGGER.info("Exception", e.getMessage());
    	
    	return new ResponseEntity<>(UtilsBean.getbody(e, http, HttpStatus.EXPECTATION_FAILED), HttpStatus.EXPECTATION_FAILED);
    }
    
    /**
     * this method catches every ErrorFormatMailException to send a custom error json response
     */
    @ExceptionHandler(ErrorFormatMailException.class)
    public ResponseEntity<Object> emailException(ErrorFormatMailException e, HttpServletRequest http) {
    	LOGGER.info("emailException", e.getMessage());
    	
    	return new ResponseEntity<>(UtilsBean.getbody(e, http, HttpStatus.CONFLICT), HttpStatus.CONFLICT);
    }
    
    /**
     * this method catches every FormatPasswordNotValid to send a custom error json response
     */
    @ExceptionHandler(FormatPasswordNotValid.class)
    public ResponseEntity<Object> passwordException(FormatPasswordNotValid e, HttpServletRequest http) {
    	LOGGER.info("passwordException", e.getMessage());
    	
    	return new ResponseEntity<>(UtilsBean.getbody(e, http, HttpStatus.CONFLICT), HttpStatus.CONFLICT);
    }
    
    /**
     * this method catches every RegisteredEmail to send a custom error json response
     */
    @ExceptionHandler(RegisteredEmailException.class)
    public ResponseEntity<Object> registeredEmail(RegisteredEmailException e, HttpServletRequest http) {
    	LOGGER.info("RegisteredEmailException", e.getMessage());
    	
        return new ResponseEntity<>(UtilsBean.getbody(e, http, HttpStatus.CONFLICT), HttpStatus.CONFLICT);
    }
    
    /**
     * this method catches every userNotFound to send a custom error json response
     */
    @ExceptionHandler(UserNotFound.class)
    public ResponseEntity<Object> userNotFound(UserNotFound e, HttpServletRequest http) {
    	LOGGER.info("UserNotFound", e.getMessage());
    	
    	return new ResponseEntity<>(UtilsBean.getbody(e,http,HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
    }
    
    /**
     * this method catches every ErrorFormatMailException to send a custom error json response
     */
    @ExceptionHandler(SaveTokenException.class)
    public ResponseEntity<Object> saveTokenException(SaveTokenException e, HttpServletRequest http) {
    	LOGGER.info("saveTokenException", e.getMessage());
    	
    	return new ResponseEntity<>(UtilsBean.getbody(e,http,HttpStatus.CONFLICT), HttpStatus.CONFLICT);
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, 
    		HttpHeaders headers, 
    		HttpStatus status, 
    		WebRequest request) {
    	LOGGER.info("HttpMessageNotReadableException:",e.getMessage());
    	return new ResponseEntity<>(UtilsBean.getbodyHandler(e,request,status), HttpStatus.BAD_REQUEST);
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException e, 
      HttpHeaders headers, 
      HttpStatus status, 
      WebRequest request) {
    	LOGGER.info("ARGUMENT NOT VALID: "+ e.getMessage());
    	return new ResponseEntity<>(UtilsBean.getbodyHandler(e,request,status), HttpStatus.BAD_REQUEST);
    }
}
