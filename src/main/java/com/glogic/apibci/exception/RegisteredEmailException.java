package com.glogic.apibci.exception;

public class RegisteredEmailException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public RegisteredEmailException() {
		super();
	}

	public RegisteredEmailException(String message) {
		super(message);
	}
}
