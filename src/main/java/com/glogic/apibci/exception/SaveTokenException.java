package com.glogic.apibci.exception;

public class SaveTokenException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SaveTokenException() {
		super();
	}

	public SaveTokenException(String message) {
		super(message);
	}
}
