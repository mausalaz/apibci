package com.glogic.apibci.exception;

public class FormatPasswordNotValid extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public FormatPasswordNotValid() {
		super();
	}

	public FormatPasswordNotValid(String message) {
		super(message);
	}

}
