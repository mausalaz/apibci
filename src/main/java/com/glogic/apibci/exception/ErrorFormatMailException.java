package com.glogic.apibci.exception;

public class ErrorFormatMailException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public ErrorFormatMailException() {
		super();
	}

	public ErrorFormatMailException(String message) {
		super(message);
	}
}
