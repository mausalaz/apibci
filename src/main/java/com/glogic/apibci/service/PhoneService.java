package com.glogic.apibci.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glogic.apibci.repository.PhoneRepository;

@Service
public class PhoneService {
	
	@Autowired
	private PhoneRepository phoneRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PhoneService.class);
	
	public void findAll() {
		phoneRepository.findAll().forEach(phone -> LOGGER.info(phone.toString()));
	}

}
