package com.glogic.apibci.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.glogic.apibci.constant.Messages;
import com.glogic.apibci.dto.HeaderResponseDTO;
import com.glogic.apibci.dto.LoginDTO;
import com.glogic.apibci.dto.UserDTO;
import com.glogic.apibci.entity.User;
import com.glogic.apibci.exception.ErrorFormatMailException;
import com.glogic.apibci.exception.FormatPasswordNotValid;
import com.glogic.apibci.exception.RegisteredEmailException;
import com.glogic.apibci.exception.SaveTokenException;
import com.glogic.apibci.exception.UserNotFound;
import com.glogic.apibci.repository.UserRepository;
import com.glogic.apibci.util.UtilsBean;

@Service
public class UserService {
	
@Autowired
private UserRepository userRepository;

@Autowired
protected Messages messages;

private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

public UserDTO save( User userBody ) throws ErrorFormatMailException, FormatPasswordNotValid, RegisteredEmailException {
	
	if ( !UtilsBean.validPass(userBody.getPassword()) ) {
		LOGGER.info("Password no corressponde con formato: "+ userBody.getPassword());
		throw new FormatPasswordNotValid(messages.getInvalidFormatPassword()); 
	}
	
//	userRepository.findAll().forEach(user -> LOGGER.info(user.toString()));
	
	if (!EmailValidator.getInstance().isValid(userBody.getEmail())) {
		throw new ErrorFormatMailException(messages.getInvalidFormatEmail()); 
	}
	
	
	Optional<User> u = userRepository.findUserByEmail (userBody.getEmail());
	if (u.isPresent()) {
		throw new RegisteredEmailException(messages.getRegisteredEmail()); 
	}
	
	userBody.setIsactivo(true);
	userBody.setCreado(LocalDate.now());
	userBody.setModificado(new Date());
	userBody.setUltimo_login(new Timestamp(System.currentTimeMillis()));
	userBody.setPassword(UtilsBean.hashPassword(userBody.getPassword()));
	userBody.setToken(UtilsBean.getJWTToken(userBody.getName()));

	userRepository.save (userBody);
	UserDTO userDTO = new UserDTO();
	userDTO.setId(userBody.getId_user());
	userDTO.setCreated(userBody.getCreado());
	userDTO.setModified(userBody.getModificado());
	userDTO.setLast_login(userBody.getUltimo_login());
	userDTO.setToken(userBody.getToken());//FIX PENDIENTE
	userDTO.setIsactive(userBody.isIsactivo());
	
	LOGGER.info("userBody.getPassword(): " + userBody.getPassword());
	LOGGER.info("password iguales?", UtilsBean.checkPassword("Mau5zalas5", userBody.getPassword()) );
	
    return userDTO;
}


public ResponseEntity<Object> findByMailAndPassword( String email, String password ) throws UserNotFound, SaveTokenException {
	Integer updatedToken = null;
     User user = userRepository.findUserByEmail (email).orElseThrow(() -> new UserNotFound("User not Found"));
     String token = null;
    	 if (user.isIsactivo()) {
    		 if (UtilsBean.checkPassword(password, user.getPassword())) {
    			  token = UtilsBean.getJWTToken(user.getName());
    			 updatedToken = userRepository.updateTokenUser(user.getId_user(), token );
			}else {
				return new ResponseEntity<>(new HeaderResponseDTO("","Usuario o password incorrecto"), HttpStatus.FORBIDDEN);
			}
		}else {
			return new ResponseEntity<>(new HeaderResponseDTO("","Usuario Inactivo"), HttpStatus.FORBIDDEN); 
		}
		
    	 if (updatedToken != null && updatedToken.intValue() == 0) {
    		 throw new SaveTokenException(messages.getErrorSaveToken());
		}
    	 
    	 User user2 = userRepository.findUserByEmail (email).orElseThrow(() -> new UserNotFound("User not Found"));
    	 LOGGER.info("USER2: " + user2.toString());
     LoginDTO loginDTO = new LoginDTO();
     LOGGER.info("Token en BBDD:" +user2.getToken());
     loginDTO.setToken(user2.getToken());
     loginDTO.setUserActive(user.isIsactivo());
     return new ResponseEntity<>(loginDTO, HttpStatus.OK);
}

public User findById( Long id ) {
    return userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
}

public User update( User user ) {
    return userRepository.save (user);
}

public void delete( Long id ) {
	 User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
     userRepository.delete (user);
}

public List<User> findAll() {
	return (List<User>) userRepository.findAll();
}


}
