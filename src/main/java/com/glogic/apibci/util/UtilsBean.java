package com.glogic.apibci.util;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import com.glogic.apibci.constant.Constantes;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class UtilsBean extends BeanUtilsBean{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsBean.class);
	
	//one minute
	private static int durationToken = 60000;
	// Define the BCrypt workload to use when generating password hashes. 10-31 is a valid value.
	private static int workload = 12;
	
	private static String secretKey = "mySecretKey";
	
	public UtilsBean() {
		super();
	}
	
	public static String getJWTToken(String username) {
		
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + durationToken))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		LOGGER.info("token:"+ token);
		return "Bearer " + token;
	}
	
	public static boolean validPass(String pass) {

		Pattern pat = Pattern.compile(Constantes.PASS_REGEX); 
		if (pass == null) 
		return false; 
		return pat.matcher(pass).matches();
	}
	
	/**
	 * This method can be used for hash password
	 * @param password_plaintext The account's plaintext password, as provided during a login request
	 * @return String - the hash password
	 */
	public static String hashPassword(String password_plaintext) {
		String salt = BCrypt.gensalt(workload);
		String hashed_password = BCrypt.hashpw(password_plaintext, salt);

		return(hashed_password);
	}

	/**
	 * This method can be used to verify a computed hash from a plaintext (e.g. during a login
	 * request) with that of a stored hash from a database. The password hash from the database
	 * must be passed as the second variable.
	 * @param password_plaintext The account's plaintext password, as provided during a login request
	 * @param stored_hash The account's stored password hash, retrieved from the authorization database
	 * @return boolean - true if the password matches the password of the stored hash, false otherwise
	 */
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;

		if(null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

		return(password_verified);
	}
			
    /**
     * this method get the body response for custom exceptionHandler
     */
    public static Map<String, Object> getbody(Exception e, HttpServletRequest http, HttpStatus status){
    	Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("message", e.getMessage());
        body.put("error", status.getReasonPhrase());
        body.put("path", http.getContextPath() + http.getServletPath());
        
        return body;
    }
    
    public static Map<String, Object> getbodyHandler(Exception e, WebRequest request, HttpStatus status){
    	Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("message", e.getMessage());
        body.put("error", status.getReasonPhrase());
        body.put("path", request.getContextPath());
        
        return body;
    }

}
