package com.glogic.apibci.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = { "com.glogic.apibci.*" })
@PropertySource("classpath:messages.properties")
public class Messages {
	
	@Value("${registered.email}")
	private String registeredEmail;

	@Value("${invalid.format.email}")
	private String invalidFormatEmail;
	
	@Value("${invalid.format.password}")
	private String invalidFormatPassword;
	
	@Value("${error.save.token}")
	private String errorSaveToken;

	public String getRegisteredEmail() {
		return registeredEmail;
	}

	public String getInvalidFormatEmail() {
		return invalidFormatEmail;
	}

	public String getInvalidFormatPassword() {
		return invalidFormatPassword;
	}

	public String getErrorSaveToken() {
		return errorSaveToken;
	}

}
