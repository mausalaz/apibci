package com.glogic.apibci.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.glogic.apibci.entity.Phone;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, Long> {

}
