package com.glogic.apibci.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.glogic.apibci.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

//	@Query("SELECT u FROM User u WHERE u.email = :email and u.password = :password")
//	Optional<User> findByMailAndPassword(@Param("email") String email, @Param("password") String password);
	
    @Query("SELECT u FROM User u WHERE u.email = :email")
    Optional<User> findUserByEmail(@Param("email") String email);
    
    @Transactional
    @Query("UPDATE User u set u.token = :token where u.id = :id")
    @Modifying(clearAutomatically = true)
	Integer updateTokenUser(@Param("id") Long id, @Param("token") String token);
    
    
}
