package com.glogic.apibci.dto;

import java.io.Serializable;

public class HeaderResponseDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	
	private String errorMessage;

	
	/**
	 * Constructor
	 */
	public HeaderResponseDTO() {
		
	}
	
	/**
	 * 
	 * @param msg
	 * @param error
	 */
	public HeaderResponseDTO(String msg, String error) {
		this.message = msg;
		this.errorMessage = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
