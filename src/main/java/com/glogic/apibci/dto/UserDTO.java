package com.glogic.apibci.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class UserDTO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private boolean isactive;
	
    private LocalDate created;
	
    private Date modified;
    
    private Date last_login;
    
    private String token;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getLast_login() {
		return last_login;
	}

	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
    
}
