package com.glogic.apibci.dto;

import java.io.Serializable;

public class LoginDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String token;
	private boolean isUserActive;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public boolean isUserActive() {
		return isUserActive;
	}
	
	public void setUserActive(boolean isUserActive) {
		this.isUserActive = isUserActive;
	}
	
}
