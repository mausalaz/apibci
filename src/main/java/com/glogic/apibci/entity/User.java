package com.glogic.apibci.entity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "USER")
public class User{

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
    private Long id_user;
	
	@NotBlank
	@Column(name="name")
    private String name;
	
	@NotBlank
	@Column(name="email")
    private String email;
	
	@NotBlank
	@Column(name="password")
    private String password;
	
	@Column(name="isactive")
    private boolean isactivo;
	
	@Column(name="created")
    private LocalDate creado;
	
	
	@Column(name="modified")
    private Date modificado;
	
	@UpdateTimestamp
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_login")
    private Date  ultimo_login;
	
	@Column(name="token")
    private String token;
	
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="user_id")
    private List<Phone> phones;

    public User() {}
    

	public Long getId_user() {
		return id_user;
	}

	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public boolean isIsactivo() {
		return isactivo;
	}

	public void setIsactivo(boolean isactivo) {
		this.isactivo = isactivo;
	}

	public LocalDate getCreado() {
		return creado;
	}

	public void setCreado(LocalDate creado) {
		this.creado = creado;
	}

	public Date getModificado() {
		return modificado;
	}

	public void setModificado(Date modificado) {
		this.modificado = modificado;
	}


	public Date getUltimo_login() {
		return ultimo_login;
	}

	public void setUltimo_login(Date ultimo_login) {
		this.ultimo_login = ultimo_login;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	@Override
    public String toString() {
        return "User{" + "id_user=" + id_user 
        			   + ", name=" + name 
        			   + ", email=" + email
        			   + ", password=" + password
        			   + ", isactivo=" + isactivo
        			   + ", creado=" + creado
        			   + ", modificado=" + modificado
        			   + ", ultimo_login=" + ultimo_login
        			   + ", token=" + token
        			   + '}';
    }

}
