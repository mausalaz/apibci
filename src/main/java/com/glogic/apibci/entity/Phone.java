package com.glogic.apibci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PHONE")
public class Phone{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_phone")
	private Long id_phone;
	
	@Column(name="number")
	private String number;
	
	@Column(name="citycode")
	private String citycode;
	
	@Column(name="contrycode")
	private String contrycode;

	
	


	public Long getId_phone() {
		return id_phone;
	}

	public void setId_phone(Long id_phone) {
		this.id_phone = id_phone;
	}

	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getCitycode() {
		return citycode;
	}
	
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public String getContrycode() {
		return contrycode;
	}
	
	public void setContrycode(String contrycode) {
		this.contrycode = contrycode;
	}
	
	
	@Override
    public String toString() {
        return "User{" + "id_phone=" + id_phone 
        			   + ", number=" + number 
        			   + ", citycode=" + citycode
        			   + ", contrycode=" + contrycode
        			   + '}';
    }
	
}
