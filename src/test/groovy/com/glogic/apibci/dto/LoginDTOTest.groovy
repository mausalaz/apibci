package com.glogic.apibci.dto
import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class LoginDTOTest extends Specification{

	
	def "testing params LoginDTO entity"()
	{
		given: "a user with his attributes"
			LoginDTO loginDTO = new LoginDTO();
			loginDTO.setUserActive(true)
			loginDTO.setToken("token")
		
		when:
			"we ask for attributes of LoginDTO"
			boolean activo = loginDTO.isUserActive()
			String token = loginDTO.getToken()
	
		then:
			"get the correct attributes of LoginDTO"
			
			activo == true
			token == "token"
	}

}
