package com.glogic.apibci.dto

import static org.junit.jupiter.api.Assertions.*

import java.time.LocalDate
import org.junit.jupiter.api.Test

import spock.lang.Specification

class UserDTOTest extends Specification{


	def "testing params user entity"()
	{
		given: "a userDTO with his attributes"
		UserDTO userDTO = new UserDTO();
		userDTO.setId(1L)
		userDTO.setIsactive(true)
		userDTO.setCreated(new LocalDate(12,06,28))
		userDTO.setModified(new Date(1220227200))
		userDTO.setLast_login(new Date(1220227200))
		userDTO.setToken("token")
		
	
		
		when:
			"we ask for attributes of userDTO"
			Long id = userDTO.getId()
			boolean activo = userDTO.isactive
			LocalDate creado = userDTO.getCreated()
			Date modificado = userDTO.getModified()
			Date ultimoLogin = userDTO.getLast_login()
			String token = userDTO.getToken()
	
		then:
			"get de correct userDTO attributes"
			id == 1L
			activo == true
			creado == new LocalDate(12,06,28)
			modificado == new Date(1220227200)
			ultimoLogin == new Date(1220227200)
			token == "token"
	}

}
