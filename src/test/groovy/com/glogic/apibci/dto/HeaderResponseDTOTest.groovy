package com.glogic.apibci.dto

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class HeaderResponseDTOTest extends Specification{

def "testing params HeaderResponseDTO DTO"()
	{
		given: "a HeaderResponseDTO with his attributes"
			HeaderResponseDTO headerResponseDTO = new HeaderResponseDTO("message", "errorMessage");
		
		when:
			"we ask for attributes of HeaderResponseDTO"
			String message = headerResponseDTO.getMessage()
			String errorMessage = headerResponseDTO.getErrorMessage()
	
		then:
			"get the correct attributes of HeaderResponseDTO"
			message == headerResponseDTO.getMessage()
			errorMessage == headerResponseDTO.getErrorMessage()
	}
	
	def "testing set params HeaderResponseDTO DTO"()
	{
		given: "a HeaderResponseDTO set his attributes"
			HeaderResponseDTO headerResponseDTO = new HeaderResponseDTO();
			headerResponseDTO.setMessage("message")
			headerResponseDTO.setErrorMessage("errorMessage")
		
		when:
			"we ask for attributes of HeaderResponseDTO"
			String message = headerResponseDTO.getMessage()
			String errorMessage = headerResponseDTO.getErrorMessage()
	
		then:
			"get the correct attributes of HeaderResponseDTO"
			message == headerResponseDTO.getMessage()
			errorMessage == headerResponseDTO.getErrorMessage()
	}

}
