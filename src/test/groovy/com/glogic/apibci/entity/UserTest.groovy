package com.glogic.apibci.entity

import org.junit.Test
import com.glogic.apibci.entity.Phone
import com.glogic.apibci.entity.User
import java.time.LocalDate
import spock.lang.Specification

class UserTest extends Specification{
	

	def "testing params user entity"()
	{
		given: "a user with his attributes"
			User user = new User();
			user.setId_user(1L)
			user.setName("isidora")
			user.setEmail("prueba@prueba.com")
			user.setPassword("secret")
			user.setIsactivo(true)
			user.setCreado(new LocalDate(2020,06,25))
			user.setModificado(new Date(1220227200))
			user.setUltimo_login(new Date(1220227200))
			user.setToken("token")
			List phones = new ArrayList<Phone>()
			Phone phone = new Phone()
			phones.add(phone)
			user.setPhones(phones)
		
		when:
			"we ask for attributes of user"
			Long id = user.getId_user()
			String nameTest = user.getName()
			String email = user.getEmail()
			String pass = user.getPassword()
			boolean activo = user.isactivo
			LocalDate creado = user.getCreado()
			Date modificado = user.getModificado()
			Date ultimoLogin = user.getUltimo_login()
			String token = user.getToken()
			List fonos = user.getPhones()
	
		then:
			"get the correct attributes"
			id == 1L
			nameTest == "isidora"
			email == "prueba@prueba.com"
			pass == "secret"
			activo == true
			creado == new LocalDate(2020,06,25)
			modificado == new Date(1220227200)
			ultimoLogin == new Date(1220227200)
			token == "token"
			fonos == phones
	}
	
	def "testing toString user"()
	{
		
		when:
			"we ask for attributes of user"
			User user = new User();
			user.setId_user(1L)
			user.setName("isidora")
			user.setEmail("prueba@prueba.com")
			user.setPassword("secret")
			user.setIsactivo(true)
			user.setCreado(new LocalDate(2020,06,25))
			user.setModificado(new Date(1220227200))
			user.setUltimo_login(new Date(1220227200))
			user.setToken("token")
		
	
		then:
			"get toString attributes"
			user.toString()
			
	}
	
}
