package com.glogic.apibci.entity

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class PhoneTest extends Specification{

	def "testing params Phone entity"()
	{
		given: "a Phone with his attributes"
		Phone phone = new Phone();
		phone.setId_phone(1L)
		phone.setNumber("+56976991769")
		phone.setCitycode("31")
		phone.setContrycode("2")
	
		
		when:
			"we ask for attributes of Phone"
			Long id = phone.getId_phone()
			String number = phone.getNumber()
			String cityCode = phone.getCitycode()
			String contryCode = phone.getContrycode()
			
	
		then:
			"get de correct attributes of Phone"
			id == 1L
			number == "+56976991769"
			cityCode == "31"
			contryCode == "2"
			
	}

}
