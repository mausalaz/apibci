package com.glogic.apibci.exception

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class ErrorFormatMailExceptionTest extends Specification{

	def "testing ErrorFormatMailException"()
	{
		given: "a ErrorFormatMailException with default constructor"
			ErrorFormatMailException errorFormatMailException = new ErrorFormatMailException();
			
	}
	
	def "testing ErrorFormatMailException with parameter"()
	{
		given: "a ErrorFormatMailException constructor with parameter message"
			ErrorFormatMailException errorFormatMailException = new ErrorFormatMailException("Formato de Email erroroneo");
			
	}

}
