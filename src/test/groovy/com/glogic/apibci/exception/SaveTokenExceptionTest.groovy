package com.glogic.apibci.exception

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class SaveTokenExceptionTest extends Specification{

	def "testing SaveTokenException"()
	{
		given: "a SaveTokenException with default constructor"
			SaveTokenException saveTokenException = new SaveTokenException();
			
	}
	
	def "testing SaveTokenException with parameter"()
	{
		given: "a SaveTokenException constructor with parameter message"
			SaveTokenException saveTokenException = new SaveTokenException("Error guardar token");
			
	}
}
