package com.glogic.apibci.exception
import static org.junit.jupiter.api.Assertions.*

import com.glogic.apibci.exception.FormatPasswordNotValid
import org.junit.jupiter.api.Test

import spock.lang.Specification

class FormatPasswordNotValidTest extends Specification{

	def "testing FormatPasswordNotValid"()
	{
		given: "a FormatPasswordNotValid with default constructor"
			FormatPasswordNotValid formatPasswordNotValid = new FormatPasswordNotValid();
			
	}
	
	def "testing FormatPasswordNotValid with parameter"()
	{
		given: "a FormatPasswordNotValid constructor with parameter message"
			FormatPasswordNotValid formatPasswordNotValid = new FormatPasswordNotValid("Formato de password no valido");
			
	}
	

}
