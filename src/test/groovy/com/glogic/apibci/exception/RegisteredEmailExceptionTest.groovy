package com.glogic.apibci.exception

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class RegisteredEmailExceptionTest extends Specification{

	def "testing RegisteredEmailException"()
	{
		given: "a RegisteredEmailException with default constructor"
			RegisteredEmailException registeredEmailException = new RegisteredEmailException();
			
	}
	
	def "testing RegisteredEmailException with parameter"()
	{
		given: "a RegisteredEmailException constructor with parameter message"
			RegisteredEmailException registeredEmailException = new RegisteredEmailException("Email registrado");
			
	}

}
