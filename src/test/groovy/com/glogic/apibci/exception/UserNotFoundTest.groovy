package com.glogic.apibci.exception

import static org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

import spock.lang.Specification

class UserNotFoundTest extends Specification{

	def "testing UserNotFound"()
	{
		given: "a UserNotFound with default constructor"
			UserNotFound userNotFound = new UserNotFound();
			
	}
	
	def "testing UserNotFound with parameter"()
	{
		given: "a UserNotFound constructor with parameter message"
			UserNotFound userNotFound = new UserNotFound("Usuario no encontrado");
			
	}

}
