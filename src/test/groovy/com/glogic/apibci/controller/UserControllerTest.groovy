package com.glogic.apibci.controller

import static org.junit.jupiter.api.Assertions.*

import groovy.json.JsonSlurper
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import com.glogic.apibci.entity.User
import com.glogic.apibci.service.UserService
import com.google.gson.Gson

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.http.HttpStatus.*
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import spock.lang.Specification

class UserControllerTest extends Specification{

	def userService = Mock(UserService)
	def userController = new UserController(userService)
	

	//The magic happens here
	MockMvc mockMvc = standaloneSetup(userController).build()

	
	def "getLogin test"() {
		given: 'response login'
		def response = mockMvc.perform(post('/login').param("email", "mg.salazar.perez@gmail.com").param("password", "mausalaz")).andReturn().response

	}
	
	def "addUser"() {
		given: 'response rrgistro'

		User user = new User()
		user.setId_user(1L)
		user.setName("daniela")
		user.setEmail("arara@gmail.com")
		user.setPassword("dani2020")
		
		Gson g = new Gson();
		String str = g.toJson(user)
		
		def response = mockMvc.perform(post('/registro').content(str).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())

	}
	
	def "getAllUsers"() {
		given: 'response allUsers'
		def response = mockMvc.perform(get('/allUsers')).andReturn().response

	}
}
