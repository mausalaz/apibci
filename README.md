Como condicion previa, se debe tener gradle y java 8 instalado en el pc

Primero se debe clonar el repositorio: abrir git bash y colocar 
git clone https://bitbucket.org/mausalaz/apibci.git

luego abrir un cmd, e ir hasta la carpta donde se descargo el proyecot, cd apibci (para ingresar a la carpeta del proyecto)

colocar gradle build (esperar a que haga build del proyecto)
al terminar colocar gradlew run para ejecutar el proyecto, se ejecutara en el puerto 8080

Dentro de la carpeta src/main/resources del proyecto se encuentra el archivo
api_bci.postman_collection.json el cual es un proyecto en postman para poder probar la aplicion y sus servicios.

Existe un metodo login que lo que hace es ver al usuario que intenta conectarse en la base de datos y obtener el token jwt si esta logeado,
ese token queda guardado en una variable para poder ser ocupado en la siguiente llamada postman (registro)
y tambien en allUsers. duracion del token 1 min, para pruebas.

ademas dentrode la carpeta resources del proycto, se encuentran los archivos solicitados:
diagrama de componentes: diagrama_componentes_uml.png
diagramas de secuencia: save_secuencia_uml.png, login_secuencia_uml.png

y tambien se ecuentra el scrip de datos que se ejecuta al iniciar la aplicacion data.sql donde se encuentra el registro de solo un usuario,
con el mail:mg.salazar.perez@gmail.com y user:admin.

A tener en cuenta: 
El campo contryCode lo mantuve asi por coherencia (supongo era countryCode).
Deje la visibilidad de log en INFO, ya que en debug arroja mucha traza, por lo mismo mis log son Info.
Los Test esta hechos al 44,3%, se me complico un poco spock con gradle, siempre he trabajado con mockito y maven, aun asi logre aprender
bastante de spock y gradle.